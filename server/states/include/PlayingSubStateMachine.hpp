//
// Created by udutta
//

#pragma once

#include "Actions.hpp"
#include "BaseStateMachine.hpp"
#include "CommonStates.hpp"
#include "Events.hpp"
#include "Guards.hpp"
#include "States.hpp"
#include <boost/mpl/vector.hpp>
#include <boost/msm/back/state_machine.hpp>
#include <boost/msm/front/completion_event.hpp>
#include <boost/msm/front/functor_row.hpp>

namespace application {
namespace server {

namespace msmf = boost::msm::front;
namespace fsmStates = application::server::states;
namespace fsmEvents = application::server::events;
namespace fsmActions = application::server::actions;
namespace fsmGuards = application::server::guards;

class PlayingSubMachineFrontend
    : public BaseStateMachine<PlayingSubMachineFrontend> {
public:
  typedef fsmStates::Song1 initial_state;

  // clang-format off
    struct transition_table : boost::mpl::vector <
    //             Start                       Event                      Next                               Action               Guard
    //+------------------------------+--------------------------+-----------------------+-----------------------------------+----------------------+
    msmf::Row < fsmStates::Song1     ,  fsmEvents::NextSong     ,    fsmStates::Song2   ,   fsmActions::start_next_song     ,    msmf::none        >,
    msmf::Row < fsmStates::Song2     ,  fsmEvents::PreviousSong ,    fsmStates::Song1   ,   fsmActions::start_previous_song ,    msmf::none        >,
    msmf::Row < fsmStates::Song2     ,  fsmEvents::NextSong     ,    fsmStates::Song3   ,   fsmActions::start_next_song     ,    msmf::none        >,
    msmf::Row < fsmStates::Song3     ,  fsmEvents::PreviousSong ,    fsmStates::Song2   ,   fsmActions::start_previous_song ,    msmf::none        >
    //+--------------------------------+-----------------------+--------------------------------+----------------------------------+-------------+
    > {};
  // clang-format on
};

using PlayingSub = boost::msm::back::state_machine<PlayingSubMachineFrontend>;

} // namespace server
} // namespace application
