//
// Created by udutta
//

#pragma once

#include "Events.hpp"
#include "LoggingState.hpp"

namespace application {
namespace server {
namespace states {

struct Stopped : public LoggingState {
  // Stopped() { std::cout << "Stopped STATE created" << std::endl; }

  template <class Event, class StateMachine>
  void on_entry(const Event &, StateMachine &stateMachine) {
    std::cout << "Entered stopped STATE" << std::endl;
  }

  template <class Event, class StateMachine>
  void on_exit(const Event &, StateMachine &stateMachine) {
    std::cout << "Exited stopped STATE" << std::endl;
  }

  struct start_playback {
    template <class Event, class StateMachine, class SourceState,
              class TargetState>
    void operator()(Event &, StateMachine &stateMachine, SourceState &,
                    TargetState &) {
      std::cout << "Performing ACTION start_playback" << std::endl;
    }
  };
};

struct Playing : public LoggingState {
  // Playing() { std::cout << "Playing STATE created" << std::endl; }

  template <class Event, class StateMachine>
  void on_entry(const Event &, StateMachine &stateMachine) {
    std::cout << "Entered playing STATE" << std::endl;
  }

  template <class Event, class StateMachine>
  void on_exit(const Event &, StateMachine &stateMachine) {
    std::cout << "Exited playing STATE" << std::endl;
  }
};

struct Open : public LoggingState {
  // Open() { std::cout << "Open STATE created" << std::endl; }

  template <class Event, class StateMachine>
  void on_entry(const Event &, StateMachine &stateMachine) {
    std::cout << "Entered open STATE" << std::endl;
  }

  template <class Event, class StateMachine>
  void on_exit(const Event &, StateMachine &stateMachine) {
    std::cout << "Exited open STATE" << std::endl;
  }
};

struct OpenWithDefer : public LoggingState {
  // Open() { std::cout << "Open STATE created" << std::endl; }
  // if the play event arrives in this state, defer it until a state handles it
  // or rejects it
  typedef boost::mpl::vector<application::server::events::Play> deferred_events;
  typedef boost::mpl::vector1<events::flags::CDLoaded> flag_list;

  template <class Event, class StateMachine>
  void on_entry(const Event &, StateMachine &stateMachine) {
    std::cout << "Entered OpenWithDefer STATE" << std::endl;
  }

  template <class Event, class StateMachine>
  void on_exit(const Event &, StateMachine &stateMachine) {
    std::cout << "Exited OpenWithDefer STATE" << std::endl;
  }
};

struct Empty : public LoggingState {
  // Empty() { std::cout << "Empty STATE created" << std::endl; }

  template <class Event, class StateMachine>
  void on_entry(const Event &, StateMachine &stateMachine) {
    std::cout << "Entered empty STATE" << std::endl;
  }

  template <class Event, class StateMachine>
  void on_exit(const Event &, StateMachine &stateMachine) {
    std::cout << "Exited empty STATE" << std::endl;
  }
};

struct EmptyWithDefer : public LoggingState {
  // Empty() { std::cout << "Empty STATE created" << std::endl; }
  // if the play event arrives in this state, defer it until a state handles it
  // or rejects it
  typedef boost::mpl::vector<application::server::events::Play> deferred_events;

  // every (optional) entry/exit methods get the event passed.
  template <class Event, class StateMachine>
  void on_entry(const Event &, StateMachine &stateMachine) {
    std::cout << "Entered EmptyWithDefer STATE" << std::endl;
  }

  template <class Event, class StateMachine>
  void on_exit(const Event &, StateMachine &stateMachine) {
    std::cout << "Exited EmptyWithDefer STATE" << std::endl;
  }
};

struct Paused : public LoggingState {
  // Paused() { std::cout << "Paused STATE created" << std::endl; }

  template <class Event, class StateMachine>
  void on_entry(const Event &, StateMachine &stateMachine) {
    std::cout << "Entered paused STATE" << std::endl;
  }

  template <class Event, class StateMachine>
  void on_exit(const Event &, StateMachine &stateMachine) {
    std::cout << "Exited paused STATE" << std::endl;
  }
};

struct Song1 : public LoggingState {
  // Song1() { std::cout << "Song1 STATE created" << std::endl; }

  template <class Event, class StateMachine>
  void on_entry(const Event &, StateMachine &stateMachine) {
    std::cout << "*** Entered song1 STATE in sub-machine ***" << std::endl;
  }

  template <class Event, class StateMachine>
  void on_exit(const Event &, StateMachine &stateMachine) {
    std::cout << "*** Exited song1 STATE in sub-machine ***" << std::endl;
  }
};

struct Song2 : public LoggingState {
  // Song2() { std::cout << "Song2 STATE created" << std::endl; }

  template <class Event, class StateMachine>
  void on_entry(const Event &, StateMachine &stateMachine) {
    std::cout << "*** Entered song2 STATE in sub-machine ***" << std::endl;
  }

  template <class Event, class StateMachine>
  void on_exit(const Event &, StateMachine &stateMachine) {
    std::cout << "*** Exited song2 STATE in sub-machine ***" << std::endl;
  }
};

struct Song3 : public LoggingState {
  // Song3() { std::cout << "Song3 STATE created" << std::endl; }

  template <class Event, class StateMachine>
  void on_entry(const Event &, StateMachine &stateMachine) {
    std::cout << "*** Entered song3 STATE in sub-machine ***" << std::endl;
  }

  template <class Event, class StateMachine>
  void on_exit(const Event &, StateMachine &stateMachine) {
    std::cout << "*** Exited song3 STATE in sub-machine ***" << std::endl;
  }
};

struct AllOk : public LoggingState {
  // AllOk() { std::cout << "AllOk STATE created" << std::endl; }

  template <class Event, class StateMachine>
  void on_entry(const Event &, StateMachine &stateMachine) {
    std::cout << "*** Entered AllOk STATE in orthogonal-machine ***"
              << std::endl;
  }

  template <class Event, class StateMachine>
  void on_exit(const Event &, StateMachine &stateMachine) {
    std::cout << "*** Exited AllOk STATE in orthogonal-machine ***"
              << std::endl;
  }
};

// this state is also made terminal so that all the events are blocked
struct ErrorMode
    : public LoggingErrorState<application::server::events::End_error> {
  // ErrorMode() { std::cout << "ErrorMode STATE created" << std::endl; }

  template <class Event, class StateMachine>
  void on_entry(const Event &, StateMachine &stateMachine) {
    std::cout << "*** Entered ErrorMode STATE in orthogonal-machine ***"
              << std::endl;
  }

  template <class Event, class StateMachine>
  void on_exit(const Event &, StateMachine &stateMachine) {
    std::cout << "*** Exited ErrorMode STATE in orthogonal-machine ***"
              << std::endl;
  }
};

} // namespace states
} // namespace server
} // namespace application
