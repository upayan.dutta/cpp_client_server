//
// Created by udutta
//

#pragma once

#include <iostream>
#include <string>

namespace application {
namespace server {
namespace events {

struct Play {
  Play() { std::cout << "### Event Play ###" << std::endl; }
};

struct Open_close {
  Open_close() { std::cout << "### Event Open_close ###" << std::endl; }
};

struct Stop {
  Stop() { std::cout << "### Event Stop ###" << std::endl; }
};

struct Cd_detected {
  // A "complicated" event type that carries some data.
  enum class DiskTypeEnum { DISK_CD = 0, DISK_DVD };

  Cd_detected(std::string name, DiskTypeEnum diskType)
      : name{name}, disc_type{diskType} {
    std::cout << "### Event Cd_detected ###" << std::endl;
  }

  Cd_detected(std::string name) : name{name} {}

  std::string name;
  DiskTypeEnum disc_type{DiskTypeEnum::DISK_DVD};
};

struct Pause {
  Pause() { std::cout << "### Event Pause ###" << std::endl; }
};

struct End_pause {
  End_pause() { std::cout << "### Event End_pause ###" << std::endl; }
};

struct NextSong {
  NextSong() { std::cout << "### Event NextSong ###" << std::endl; }
};

struct PreviousSong {
  PreviousSong() { std::cout << "### Event PreviousSong ###" << std::endl; }
};

struct Error_found {
  Error_found() { std::cout << "### Event Error_found ###" << std::endl; }
};

struct End_error {
  End_error() { std::cout << "### Event End_error ###" << std::endl; }
};

namespace flags {
struct PlayingPaused {};
struct CDLoaded {};
struct FirstSongPlaying {};
} // namespace flags

} // namespace events
} // namespace server
} // namespace application
