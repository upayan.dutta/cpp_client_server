//
// Created by udutta
//

#pragma once

#include "Events.hpp"
#include <iostream>

namespace application {
namespace server {
namespace guards {

struct good_disk_format {

  template <class Event, class StateMachine, class SourceState,
            class TargetState>
  bool operator()(Event &event, StateMachine &stateMachine, SourceState &,
                  TargetState &) {
    std::cout << "*** In guard good_disk_format ***" << std::endl;
    if (event.disc_type !=
        application::server::events::Cd_detected::DiskTypeEnum::DISK_CD) {
      std::cout << "Wrong disk, sorry" << std::endl;
      return false;
    }
    return true;
  }
};

struct auto_start {

  template <class Event, class StateMachine, class SourceState,
            class TargetState>
  bool operator()(Event &event, StateMachine &stateMachine, SourceState &,
                  TargetState &) {
    std::cout << "*** In guard auto_start ***" << std::endl;
    return false;
  }
};

struct internal_guard {

  template <class Event> bool operator()(Event const &) {
    std::cout << "*** In guard internal_guard ***" << std::endl;
    return false;
  }
};

} // namespace guards
} // namespace server
} // namespace application
