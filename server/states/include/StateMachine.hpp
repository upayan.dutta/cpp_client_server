//
// Created by udutta
//

#pragma once

#include "Actions.hpp"
#include "BaseStateMachine.hpp"
#include "CommonStates.hpp"
#include "Events.hpp"
#include "Guards.hpp"
#include "States.hpp"
#include <boost/mpl/vector.hpp>
#include <boost/msm/back/state_machine.hpp>
#include <boost/msm/front/completion_event.hpp>
#include <boost/msm/front/functor_row.hpp>

namespace application {
namespace server {

namespace msmf = boost::msm::front;
namespace fsmStates = application::server::states;
namespace fsmEvents = application::server::events;
namespace fsmActions = application::server::actions;
namespace fsmGuards = application::server::guards;

class MusicPlayerStateMachineFrontend
    : public BaseStateMachine<MusicPlayerStateMachineFrontend>

{
public:
  typedef fsmStates::Empty initial_state;

  // clang-format off
    struct transition_table : boost::mpl::vector <
    //             Start                       Event                      Next                               Action              Guard
    //+--------------------------------+-----------------------+--------------------------------+-------------------------+----------------------+
    msmf::Row < fsmStates::Stopped     ,  fsmEvents::Play       ,    fsmStates::Playing   ,   fsmStates::Stopped::start_playback  ,    msmf::none        >,
    msmf::Row < fsmStates::Stopped     ,  fsmEvents::Open_close ,    fsmStates::Open      ,   fsmActions::open_drawer     ,    msmf::none        >,
    msmf::Row < fsmStates::Stopped     ,  fsmEvents::Stop       ,    fsmStates::Stopped   ,   msmf::none                  ,    msmf::none        >,
    //+--------------------------------+-----------------------+--------------------------------+-------------------------+----------------------+
    msmf::Row < fsmStates::Open        ,  fsmEvents::Open_close  ,   fsmStates::Empty     ,   fsmActions::close_drawer    ,    msmf::none       >,
    //+--------------------------------+-----------------------+--------------------------------+-------------------------+----------------------+
    msmf::Row < fsmStates::Empty       ,  fsmEvents::Open_close  ,   fsmStates::Open      ,   fsmActions::open_drawer     ,    msmf::none       >,
    msmf::Row < fsmStates::Empty       ,  fsmEvents::Cd_detected ,   fsmStates::Stopped   ,   fsmActions::store_cd_info   ,    fsmGuards::good_disk_format >,
    msmf::Row < fsmStates::Empty       ,  fsmEvents::Cd_detected ,   fsmStates::Playing   ,   fsmActions::store_cd_info   ,    fsmGuards::auto_start       >,
    //+--------------------------------+-----------------------+--------------------------------+-------------------------+----------------------+
    msmf::Row < fsmStates::Playing     ,  fsmEvents::Stop        ,   fsmStates::Stopped   ,   fsmActions::stop_playback   ,    msmf::none       >,
    msmf::Row < fsmStates::Playing     ,  fsmEvents::Pause       ,   fsmStates::Paused    ,   fsmActions::pause_playback  ,    msmf::none       >,
    msmf::Row < fsmStates::Playing     ,  fsmEvents::Open_close  ,   fsmStates::Open      ,   fsmActions::stop_and_open   ,    msmf::none       >,
    //+--------------------------------+-----------------------+--------------------------------+-------------------------+----------------------+
    msmf::Row < fsmStates::Paused      ,  fsmEvents::End_pause   ,   fsmStates::Playing   ,   fsmActions::resume_playback ,    msmf::none       >,
    msmf::Row < fsmStates::Paused      ,  fsmEvents::Stop        ,   fsmStates::Stopped   ,   fsmActions::stop_playback   ,    msmf::none       >,
    msmf::Row < fsmStates::Paused      ,  fsmEvents::Open_close  ,   fsmStates::Open      ,   fsmActions::stop_and_open   ,    msmf::none       >
    > {};
  // clang-format on
};

using MusicPlayer =
    boost::msm::back::state_machine<MusicPlayerStateMachineFrontend>;

} // namespace server
} // namespace application
