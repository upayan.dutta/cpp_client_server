//
// Created by udutta
//

#pragma once

#include "Actions.hpp"
#include "BaseStateMachine.hpp"
#include "CommonStates.hpp"
#include "Events.hpp"
#include "Guards.hpp"
#include "PlayingSubStateMachine.hpp"
#include "States.hpp"
#include <boost/mpl/vector.hpp>
#include <boost/msm/back/state_machine.hpp>
#include <boost/msm/front/completion_event.hpp>
#include <boost/msm/front/functor_row.hpp>

namespace application {
namespace server {

namespace msmf = boost::msm::front;
namespace fsmStates = application::server::states;
namespace fsmEvents = application::server::events;
namespace fsmActions = application::server::actions;
namespace fsmGuards = application::server::guards;
namespace subFsm = application::server;

class MusicPlayerStateMachineWithOrthogonalRegionFrontend
    : public BaseStateMachine<
          MusicPlayerStateMachineWithOrthogonalRegionFrontend>

{
public:
  // the initial state of the player SM. Must be defined
  typedef boost::mpl::vector<fsmStates::EmptyWithDefer, fsmStates::AllOk>
      initial_state;

  // clang-format off
    struct transition_table : boost::mpl::vector <
    //             Start                       Event                      Next                               Action              Guard
    //+-----------------------------------+-------------------------+-----------------------------+------------------------------------------------+
    msmf::Row < fsmStates::Stopped        ,  fsmEvents::Play        ,    subFsm::PlayingSub       ,   fsmStates::Stopped::start_playback  ,    msmf::none >,
    msmf::Row < fsmStates::Stopped        ,  fsmEvents::Open_close  ,    fsmStates::OpenWithDefer ,   fsmActions::open_drawer     ,    msmf::none        >,
    msmf::Row < fsmStates::Stopped        ,  fsmEvents::Stop        ,    fsmStates::Stopped       ,   fsmActions::stopped_again   ,    msmf::none        >,
    //+-----------------------------------+-------------------------+-----------------------------+----------------------------------+-------------+
    msmf::Row < fsmStates::OpenWithDefer  ,  fsmEvents::Open_close  ,   fsmStates::EmptyWithDefer ,   fsmActions::close_drawer    ,    msmf::none       >,
    //+-----------------------------------+-------------------------+-----------------------------+------------------------------------------------+
    msmf::Row < fsmStates::EmptyWithDefer ,  fsmEvents::Open_close  ,   fsmStates::OpenWithDefer  ,   fsmActions::open_drawer     ,    msmf::none       >,
    msmf::Row < fsmStates::EmptyWithDefer ,  fsmEvents::Cd_detected ,   fsmStates::Stopped        ,   fsmActions::store_cd_info   ,    msmf::none       >,
    //+-----------------------------------+-------------------------+-----------------------------+------------------------------------------------+
    msmf::Row < subFsm::PlayingSub        ,  fsmEvents::Stop        ,   fsmStates::Stopped        ,   fsmActions::stop_playback   ,    msmf::none       >,
    msmf::Row < subFsm::PlayingSub        ,  fsmEvents::Pause       ,   fsmStates::Paused         ,   fsmActions::pause_playback  ,    msmf::none       >,
    msmf::Row < subFsm::PlayingSub        ,  fsmEvents::Open_close  ,   fsmStates::OpenWithDefer  ,   fsmActions::stop_and_open   ,    msmf::none       >,
    //+-----------------------------------+-------------------------+-----------------------------+------------------------------------------------+
    msmf::Row < fsmStates::Paused         ,  fsmEvents::End_pause   ,   subFsm::PlayingSub        ,   fsmActions::resume_playback ,    msmf::none       >,
    msmf::Row < fsmStates::Paused         ,  fsmEvents::Stop        ,   fsmStates::Stopped        ,   fsmActions::stop_playback   ,    msmf::none       >,
    msmf::Row < fsmStates::Paused         ,  fsmEvents::Open_close  ,   fsmStates::OpenWithDefer  ,   fsmActions::stop_and_open   ,    msmf::none       >,
    //+-----------------------------------+-------------------------+-----------------------------+------------------------------------------------+
    msmf::Row < fsmStates::AllOk          ,  fsmEvents::Error_found ,   fsmStates::ErrorMode      ,   fsmActions::report_error    ,    msmf::none       >,
    msmf::Row < fsmStates::ErrorMode      ,  fsmEvents::End_error   ,   fsmStates::AllOk          ,   fsmActions::report_end_error,    msmf::none       >
    > {};
  // clang-format on
};

using MusicPlayerWithOrthogonalRegion = boost::msm::back::state_machine<
    MusicPlayerStateMachineWithOrthogonalRegionFrontend>;

} // namespace server
} // namespace application
