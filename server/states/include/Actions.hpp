//
// Created by udutta
//

#pragma once

#include "Events.hpp"
#include <iostream>

namespace application {
namespace server {
namespace actions {

struct start_playback {

  template <class Event, class StateMachine, class SourceState,
            class TargetState>
  void operator()(Event &, StateMachine &stateMachine, SourceState &,
                  TargetState &) {}
};

struct open_drawer {

  template <class Event, class StateMachine, class SourceState,
            class TargetState>
  void operator()(Event &, StateMachine &stateMachine, SourceState &,
                  TargetState &) {
    std::cout << "<==== Performed ACTION open_drawer ====>" << std::endl;
  }
};

struct close_drawer {

  template <class Event, class StateMachine, class SourceState,
            class TargetState>
  void operator()(Event &, StateMachine &stateMachine, SourceState &,
                  TargetState &) {
    std::cout << "<==== Performed ACTION close_drawer ====>" << std::endl;
  }
};

struct store_cd_info {

  template <class Event, class StateMachine, class SourceState,
            class TargetState>
  void operator()(Event &, StateMachine &stateMachine, SourceState &,
                  TargetState &) {
    std::cout << "<==== Performed ACTION store_cd_info ====>" << std::endl;
  }
};

struct stop_playback {

  template <class Event, class StateMachine, class SourceState,
            class TargetState>
  void operator()(Event &, StateMachine &stateMachine, SourceState &,
                  TargetState &) {
    std::cout << "<==== Performed ACTION stop_playback ====>" << std::endl;
  }
};

struct pause_playback {

  template <class Event, class StateMachine, class SourceState,
            class TargetState>
  void operator()(Event &, StateMachine &stateMachine, SourceState &,
                  TargetState &) {
    std::cout << "<==== Performed ACTION pause_playback ====>" << std::endl;
  }
};

struct stop_and_open {

  template <class Event, class StateMachine, class SourceState,
            class TargetState>
  void operator()(Event &, StateMachine &stateMachine, SourceState &,
                  TargetState &) {
    std::cout << "<==== Performed ACTION stop_and_open ====>" << std::endl;
  }
};

struct resume_playback {

  template <class Event, class StateMachine, class SourceState,
            class TargetState>
  void operator()(Event &, StateMachine &stateMachine, SourceState &,
                  TargetState &) {
    std::cout << "<==== Performed ACTION resume_playback ====>" << std::endl;
  }
};

struct stopped_again {

  template <class Event, class StateMachine, class SourceState,
            class TargetState>
  void operator()(Event &, StateMachine &stateMachine, SourceState &,
                  TargetState &) {
    std::cout << "<==== Performed ACTION stopped_again ====>" << std::endl;
  }
};

struct start_next_song {

  template <class Event, class StateMachine, class SourceState,
            class TargetState>
  void operator()(Event &, StateMachine &stateMachine, SourceState &,
                  TargetState &) {
    std::cout << "<==== Performed ACTION start_next_song ====>" << std::endl;
  }
};

struct start_previous_song {

  template <class Event, class StateMachine, class SourceState,
            class TargetState>
  void operator()(Event &, StateMachine &stateMachine, SourceState &,
                  TargetState &) {
    std::cout << "<==== Performed ACTION start_previous_song ====>"
              << std::endl;
  }
};

struct report_error {

  template <class Event, class StateMachine, class SourceState,
            class TargetState>
  void operator()(Event &, StateMachine &stateMachine, SourceState &,
                  TargetState &) {
    std::cout << "<==== Performed ACTION report_error ====>" << std::endl;
  }
};

struct report_end_error {

  template <class Event, class StateMachine, class SourceState,
            class TargetState>
  void operator()(Event &, StateMachine &stateMachine, SourceState &,
                  TargetState &) {
    std::cout << "<==== Performed ACTION report_end_error ====>" << std::endl;
  }
};

struct internal_action {

  template <class Event> void operator()(Event const &) {
    std::cout << "<==== Performed ACTION internal_actions ====>" << std::endl;
  }
};

} // namespace actions
} // namespace server
} // namespace application
