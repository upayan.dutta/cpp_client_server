//
// Created by udutta
//

#pragma once

#include <boost/core/demangle.hpp>
#include <boost/msm/front/state_machine_def.hpp>
#include <iostream>
#include <typeinfo>

namespace application {
namespace server {

template <typename Machine>
class BaseStateMachine : public boost::msm::front::state_machine_def<Machine> {

protected:
  template <class StateMachine, class Event>
  void no_transition(const Event &event, StateMachine &, int state) {
    std::cout << "No transition from state " << state << " on event "
              << typeid(event).name() << std::endl;
  }

  template <class StateMachine, class Event>
  void exception_caught(Event const &, StateMachine &machine,
                        std::exception &e) {
    std::cout << "Exception " << boost::core::demangle(typeid(e).name())
              << " caught in " << e.what()
              << boost::core::demangle(typeid(machine).name()) << std::endl;
    throw;
  }
};

} // namespace server
} // namespace application