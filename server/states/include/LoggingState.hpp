//
// Created by udutta
//

#pragma once

#include <boost/msm/front/states.hpp>
#include <iostream>

namespace application {
namespace server {
template <class Machine> std::string procedureName();

struct LoggingState : boost::msm::front::state<> {
  virtual ~LoggingState() = default;

  template <class Event, class StateMachine>
  void on_entry(const Event &, StateMachine &) {
    //    std::cout << "Entering state: " << state << std::endl;
  }

  template <class Event, class StateMachine>
  void on_exit(const Event &, StateMachine &) {
    //    std::cout << "Leaving state: " << state << std::endl;
  }

private:
  std::string state;
};

template <typename Interrupt_Event>
struct LoggingErrorState
    : // public msm::front::terminate_state<> // ErrorMode terminates the state
      // machine
      public boost::msm::front::interrupt_state<
          Interrupt_Event                        /*end_error*/
          /*mpl::vector<end_error,end_error2>*/> // ErroMode just
                                                 // interrupts. Will
                                                 // resume if
                                                 // the event
                                                 // end_error is
                                                 // generated
{
  virtual ~LoggingErrorState() = default;

  template <class Event, class StateMachine>
  void on_entry(const Event &, StateMachine &) {
    //    std::cout << "Entering state: " << state << std::endl;
  }

  template <class Event, class StateMachine>
  void on_exit(const Event &, StateMachine &) {
    //    std::cout << "Leaving state: " << state << std::endl;
  }

private:
  std::string state;
}; // namespace server
} // namespace server
} // namespace application