//
// Created by udutta
//

#pragma once

#include "LoggingState.hpp"

namespace application {
namespace server {

struct Init : public LoggingState {};

} // namespace server
} // namespace application
