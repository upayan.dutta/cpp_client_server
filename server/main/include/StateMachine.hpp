//
// Created by udutta on 9/15/19.
//

#pragma once

#include <boost/mpl/vector.hpp>
#include <boost/msm/back/state_machine.hpp>
#include <boost/msm/front/completion_event.hpp>
#include <boost/msm/front/functor_row.hpp>

namespace application {
namespace server {}
} // namespace application
