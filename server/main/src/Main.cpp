#include "Main.hpp"
#include "Test.hpp"
#include <iostream>
#include <stdlib.h>

int main(int argc, char **argv) {
  std::cout << "Hello world!! Today I am born." << std::endl;

  // tests::server::startSimpleFsmTest();
  if (2 != argc)
    return 0;

  tests::server::startTest(static_cast<tests::server::testType>(atoi(argv[1])));

  return 0;
}