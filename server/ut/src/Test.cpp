//
// Created by udutta
//

#include "Test.hpp"
#include "Events.hpp"
#include "StateMachine.hpp"
#include "StateMachineWithInternalTransition.hpp"
#include "StateMachineWithOrthogonalRegion.hpp"
#include "StateMachineWithSubState.hpp"

namespace tests {
namespace server {

// Test for simple FSM
void startSimpleFsmTest() {
  namespace events = application::server::events;
  application::server::MusicPlayer fsm;

  fsm.start();
  fsm.process_event(events::Open_close{});
  fsm.process_event(events::Open_close{});

  // will be rejected, wrong disk type
  fsm.process_event(events::Cd_detected(
      "louie, louie", events::Cd_detected::DiskTypeEnum::DISK_DVD));
  fsm.process_event(events::Cd_detected(
      "louie, louie", events::Cd_detected::DiskTypeEnum::DISK_CD));
  fsm.process_event(events::Play{});

  // at this point, Play is active
  fsm.process_event(events::Pause{});

  // go back to Playing
  fsm.process_event(events::End_pause{});

  fsm.process_event(events::Pause{});

  fsm.process_event(events::Stop{});

  // event leading to the same state
  // no action method called as it is not present in the transition table
  fsm.process_event(events::Stop{});

  std::cout << "stop fsm" << std::endl;
  fsm.stop();
}

// Test for FSM with a sub-FSM as state
void startSubFsmTest() {
  namespace events = application::server::events;
  application::server::MusicPlayerWithSubMachine fsm;

  // needed to start the highest-level SM. This will call on_entry and mark the
  // start of the SM
  fsm.start();
  // go to Open, call on_exit on Empty, then action, then on_entry on Open
  fsm.process_event(events::Open_close{});
  fsm.process_event(events::Open_close{});
  fsm.process_event(events::Cd_detected{"louie, louie"});
  fsm.process_event(events::Play{});
  std::cout << "#### Should enter sub-machine ###" << std::endl;

  //**********************************************************************************//
  // at this point, Play is active
  // make transition happen inside it. Player has no idea about this event but
  // it's ok.
  fsm.process_event(events::NextSong{});
  fsm.process_event(events::NextSong{});
  fsm.process_event(events::PreviousSong{});
  //**********************************************************************************//

  fsm.process_event(events::Pause{});
  // go back to Playing
  // as you see, it starts back from the original state
  fsm.process_event(events::End_pause{});
  fsm.process_event(events::Pause{});
  fsm.process_event(events::Stop{});

  // event leading to the same state
  fsm.process_event(events::Stop{});

  // stop the fsm (call on_exit's, including the submachines)
  fsm.process_event(events::Play{});
  std::cout << "stop fsm" << std::endl;
  fsm.stop();
  std::cout << "restart fsm" << std::endl;
  fsm.start();
}

// test for orthogonal region
void startOrthogonalRegionTest() {
  namespace events = application::server::events;
  application::server::MusicPlayerWithOrthogonalRegion fsm;

  // needed to start the highest-level SM. This will call on_entry and mark the
  // start of the SM
  fsm.start();

  // test deferred event
  // deferred in Empty and Open, will be handled only after event cd_detected
  fsm.process_event(events::Play{});
  std::cout << "**************************************" << '\n';

  // go to Open, call on_exit on Empty, then action, then on_entry on Open
  fsm.process_event(events::Open_close{});
  std::cout << "**************************************" << '\n';
  fsm.process_event(events::Open_close{});
  std::cout << "**************************************" << '\n';
  fsm.process_event(
      events::Cd_detected{"louie, louie"}); // Move to Stopped state
  std::cout << "**************************************" << '\n';

  // make transition happen inside it. Player has no idea about this event but
  // it's ok.
  // Note: After moving to Stopped state, the deferred Play event will be fired
  // and the state will move to subFsm::PlayingSub
  fsm.process_event(events::NextSong{}); // In sub-FSM
  std::cout << "**************************************" << '\n';
  fsm.process_event(events::NextSong{}); // In sub-FSM
  std::cout << "**************************************" << '\n';
  fsm.process_event(events::PreviousSong{}); // In sub-FSM
  std::cout << "**************************************" << '\n';

  fsm.process_event(events::Pause{}); // Move to Paused state
  std::cout << "**************************************" << '\n';

  // go back to Playing
  // as you see, it starts back from the original state
  fsm.process_event(events::End_pause{}); // Move to state PlayingSub
  std::cout << "**************************************" << '\n';
  fsm.process_event(events::Pause{}); // Move to state Paused
  std::cout << "**************************************" << '\n';
  fsm.process_event(events::Stop{}); // Move to state Stopped
  std::cout << "**************************************" << '\n';

  // event leading to the same state
  fsm.process_event(events::Stop{}); // Move to same state Stopped
  std::cout << "**************************************" << '\n';

  // event leading to a terminal/interrupt state
  fsm.process_event(
      events::Error_found{}); // This will trigger orthogonal FSM- ErrorMode
  std::cout << "**************************************" << '\n';

  // try generating more events
  std::cout << "Trying to generate another event"
            << std::endl; // will not work, fsm is terminated or interrupted

  fsm.process_event(
      events::Play{}); // This event will be interrupted as ErrorMode state is
                       // msm::front::interrupt_state<events::End_error>
  std::cout << "Trying to end the error"
            << std::endl; // will work only if ErrorMode is interrupt state
  std::cout << "**************************************" << '\n';

  fsm.process_event(events::End_error{}); // This will move to state AllOk
  std::cout << "Trying to generate another event"
            << std::endl; // will work only if ErrorMode is interrupt state
  std::cout << "**************************************" << '\n';

  fsm.process_event(events::Play{}); // Currently in state Stopped (line #137).
                                     // Will move to subFsm::PlayingSub
  std::cout << "**************************************" << '\n';
  std::cout << "stop fsm" << std::endl;
  fsm.stop();
}

void startTest(testType type) {
  switch (type) {
  case testType::SIMPLE_FSM:
    std::cout << "Running simple FSM test" << std::endl;
    startSimpleFsmTest();
    break;
  case testType::SUB_FSM:
    std::cout << "Running FSM with sub-machine test" << std::endl;
    startSubFsmTest();
    break;
  case testType::ORTHOGONAL_REGION:
    std::cout << "Running FSM with orthogonal region test" << std::endl;
    startOrthogonalRegionTest();
    break;
  default:
    std::cout << "!!! Incorrect test type passed !!!" << std::endl;
  }
}

} // namespace server
} // namespace tests