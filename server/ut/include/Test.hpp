//
// Created by udutta
//

#pragma once

namespace tests {
namespace server {

enum class testType { SIMPLE_FSM, SUB_FSM, ORTHOGONAL_REGION };

void startTest(testType);
} // namespace server
} // namespace tests
